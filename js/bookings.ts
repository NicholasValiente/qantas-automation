import { Selector, ClientFunction } from "testcafe";

fixture`Test Booking Features`
    .page`https://www.phptravels.net/`;

const getLocation = ClientFunction(() => document.location.href);
test("Book a Hotel", async t => {

    //click hotels button in navbar & check that it went to the right page
    const hotelsNav = Selector("a.loader").withAttribute("title", "Hotels");
    await t
        .click(hotelsNav)
        .expect(getLocation()).contains("https://www.phptravels.net/hotels");


    // find the first listing with "Hotel" in its name and click through to its page
    // would prefer to use the hotel filter, but it is broken
    const hotelTitles = elemntsWithClassName("RTL go-text-right mt0 mb4 list_title");
    for (var i = 0; i < await hotelTitles.count; i++) {
        var element = hotelTitles.nth(i).find("a");
        let title = await element.innerText;
        if (title.includes("Hotel")) {
            await t
                .click(element);
            break;
        }
    }
    //then check that it went to a hotel details page
    await t
        .expect(getLocation()).contains("https://www.phptravels.net/hotels/detail/");


    //set the start and end dates fo be from christas eve to christmas day & check that they were set in the page URL
    const checkInDate = await Selector(".dpd1rooms");
    const checkOutDate = await Selector(".dpd2rooms");
    const updateButton = await Selector("input.btn");

    await t
        .selectText(checkInDate)
        .pressKey('delete')
        .typeText(checkInDate, "24/12/2018")
        .selectText(checkOutDate)
        .pressKey('delete')
        .typeText(checkOutDate, "25/12/2018")
        .click(updateButton)
        .expect(getLocation()).contains("24-12-2018/25-12-2018/2/0");


    // click book now and go to the checkout page
    //currently broken due to an issue with testcafe, ticket can be found at:
    //      https://github.com/DevExpress/testcafe/issues/2479
    const bookNowButton = await Selector(".bgwhite > tbody:nth-child(7) > tr:nth-child(1) > td:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) > button:nth-child(1)");
    // const bookingButton = await Selector("div[class=\"col-md-4 go-right\"]").find("button").nth(0);
    const rooms = await Selector(".bgwhite");

    console.debug(await bookNowButton.visible);
    console.debug(await bookNowButton.attributes);
    await t
        .hover(rooms)
        .expect(bookNowButton.exists).ok

    console.debug(await bookNowButton.visible);
    await t
        // .debug()
        .click(bookNowButton);

    console.debug("button aparently clicked");


    //fill out details for guest checkout and submit, before checking browser was properly redirected to invoice page
    const FNameField = await Selector("input.form-control").withAttribute("name", "firstname");
    const LNameField = await Selector("input.form-control").withAttribute("name", "lastname");
    const emailField = await Selector("input.form-control").withAttribute("name", "email");
    const confirmEmailField = await Selector("input.form-control").withAttribute("name", "confirmemail");
    const mobileField = await Selector("input.form-control").withAttribute("name", "phone");
    const addressField = await Selector("input.form-control").withAttribute("name", "address");
    const countryDropdown = await Selector("#s2id_autogen2");
    const countryField = await Selector("span.select2-chosen");
    const countries = await Selector("li.select2-results-dept-0 > div");
    const bookingButton = await Selector("button.completebook");


    await t
        .typeText(FNameField, "John")
        .expect(FNameField.value).eql("John")

        .typeText(LNameField, "Smith")
        .expect(LNameField.value).eql("Smith")

        .typeText(emailField, "jsmith@fakemail.com")
        .expect(emailField.value).eql("jsmith@fakemail.com")

        .typeText(confirmEmailField, "jsmith@fakemail.com")
        .expect(confirmEmailField.value).eql("jsmith@fakemail.com")
        // .expect(emailField.value).eql(confirmEmailField.value)

        .typeText(mobileField, "0412344321")
        .expect(mobileField.value).eql("0412344321")

        .typeText(addressField, "123 Fake St")
        .expect(addressField.value).eql("123 Fake St")

        //for some reason this one looks like its going to time out before working
        .click(countryDropdown)
        .click(countries.withText("Australia"))
        .expect(countryField.innerText).eql("Australia")

        .click(bookingButton)

        .expect(getLocation()).contains("/invoice?id=");

});



test("Book a flight", async t => {

    //click through to flights page using the navigation bar & check that page was properly redirected
    const flightsNav = Selector("a.loader").withAttribute("title", "Flights");

    await t
        .click(flightsNav)
        .expect(getLocation()).contains("/flights");


    //find the first flight available and click  its book now button, before checking that the page was properly redirected
    const firstFlight = await Selector("td.wow").nth(0);
    const firstBookingButton = await firstFlight.find("button.bookbtn");

    await t
        .click(firstBookingButton)
        .expect(getLocation()).contains("/flights/book/");


    //fill out details for guest checkout and submit, before checking browser was properly redirected to invoice page
    const FNameField = await Selector("input.form-control").withAttribute("name", "firstname");
    const LNameField = await Selector("input.form-control").withAttribute("name", "lastname");
    const emailField = await Selector("input.form-control").withAttribute("name", "email");
    const confirmEmailField = await Selector("input.form-control").withAttribute("name", "confirmemail");
    const mobileField = await Selector("input.form-control").withAttribute("name", "phone");
    const addressField = await Selector("input.form-control").withAttribute("name", "address");
    const countryDropdown = await Selector("#s2id_autogen2");
    const countryField = await Selector("span.select2-chosen");
    const countries = await Selector("li.select2-results-dept-0 > div");
    const bookingButton = await Selector("button.completebook");


    await t
        .typeText(FNameField, "John")
        .expect(FNameField.value).eql("John")

        .typeText(LNameField, "Smith")
        .expect(LNameField.value).eql("Smith")

        .typeText(emailField, "jsmith@fakemail.com")
        .expect(emailField.value).eql("jsmith@fakemail.com")

        .typeText(confirmEmailField, "jsmith@fakemail.com")
        .expect(confirmEmailField.value).eql("jsmith@fakemail.com")
        // .expect(emailField.value).eql(confirmEmailField.value)

        .typeText(mobileField, "0412344321")
        .expect(mobileField.value).eql("0412344321")

        .typeText(addressField, "123 Fake St")
        .expect(addressField.value).eql("123 Fake St")

        //for some reason this one looks like its going to time out before working
        .click(countryDropdown)
        .click(countries.withText("Australia"))
        .expect(countryField.innerText).eql("Australia")

        .click(bookingButton)

        .expect(getLocation()).contains("/invoice?id=");

});



test("Book a tour", async t => {

    //click through to tours page via the navigation bar, before checking that the page was redirected properly
    const toursNav = Selector("a.loader").withAttribute("title", "Tours");

    await t
        .click(toursNav)
        .expect(getLocation()).contains("/tours");


    //select the first available tour and grab its name, before trimming any elipsies off the end 
    //and replacing spaces with dashes, then finaly use the modified title string to check proper redirection
    const firstTour = await Selector("td.wow").nth(0);
    const firstTourButon = await firstTour.find("button.btn-block");
    const title = await firstTour.find("h4 > a > b");
    let tourName = await title.innerText;
    if (tourName.includes("…")) {
        tourName = tourName.replace("…", "");
    }
    tourName = tourName.replace(new RegExp(" ", 'g'), "-");

    await t
        .click(firstTourButon)
        .expect(getLocation()).contains(tourName)


    // select the desired tour date and click submit, before checking the page url to see if the date appears
    // and then click through to the booking page
    const bookingPannel = await Selector("div.panel:nth-child(2) > div:nth-child(2)");
    const datePicker = await bookingPannel.find("input").withAttribute("name", "date");
    const dateSubmit = await Selector("button.pull-right");
    const bookButton = await Selector("button.btn-action");

    await t
        .hover(bookingPannel)
        .click(datePicker)
        .selectText(datePicker)
        .pressKey("delete")
        .typeText(datePicker, "10/10/2018")
        .click(dateSubmit)
        .expect(getLocation()).contains("date=10%2F10%2F2018")
        .click(bookButton)
        .expect(getLocation()).contains("/tours/book/", tourName);


    //fill out details for guest checkout and submit, before checking browser was properly redirected to invoice page
    const FNameField = await Selector("input.form-control").withAttribute("name", "firstname");
    const LNameField = await Selector("input.form-control").withAttribute("name", "lastname");
    const emailField = await Selector("input.form-control").withAttribute("name", "email");
    const confirmEmailField = await Selector("input.form-control").withAttribute("name", "confirmemail");
    const mobileField = await Selector("input.form-control").withAttribute("name", "phone");
    const addressField = await Selector("input.form-control").withAttribute("name", "address");
    const countryDropdown = await Selector("#s2id_autogen2");
    const countryField = await Selector("span.select2-chosen");
    const countries = await Selector("li.select2-results-dept-0 > div");
    const bookingButton = await Selector("button.completebook");

    await t
        .typeText(FNameField, "John")
        .expect(FNameField.value).eql("John")

        .typeText(LNameField, "Smith")
        .expect(LNameField.value).eql("Smith")

        .typeText(emailField, "jsmith@fakemail.com")
        .expect(emailField.value).eql("jsmith@fakemail.com")

        .typeText(confirmEmailField, "jsmith@fakemail.com")
        .expect(confirmEmailField.value).eql("jsmith@fakemail.com")
        // .expect(emailField.value).eql(confirmEmailField.value)

        .typeText(mobileField, "0412344321")
        .expect(mobileField.value).eql("0412344321")

        .typeText(addressField, "123 Fake St")
        .expect(addressField.value).eql("123 Fake St")

        //for some reason this one looks like its going to time out before working
        .click(countryDropdown)
        .click(countries.withText("Australia"))
        .expect(countryField.innerText).eql("Australia")

        .click(bookingButton)

        .expect(getLocation()).contains("/invoice?id=");
});

const elemntsWithClassName = Selector(value => {
    return document.getElementsByClassName(value);
});




// li.main-lnk:nth-child(2) > a:nth-child(1)