#REST API Automation#
To run the REST API automation test cases ensure maven and java jre 1.8.0 or higher are installed using the commands:

`java -version`

`mvn -v`

If you are missing maven install it from the[ Apache maven website](https://maven.apache.org/install.html)

Once all the required libraries are installed, navigate to the project directory in terminal or command prompt and run the command:

`mvn test`


If you would like to run the tests with headless chrome edit the file src/test/javaStepDefinitions/StepDefinitions.java and uncomment ```options.addArguments("--headless");``` on line 39.

***
#Web Automation#
To run the Web Automation test cases ensure that you have nodejs with npm and testcafe installed using the commands:

`node -v`

`npm -v`

`testcafe -v`


If you are missing nodejs install it from the [nodejs website](https://nodejs.org/en)

If you are missing testcafe install it with the command:
`npm install -g testcafe`

Once all the required libraries are installed, navigate to the project directory in terminal or command prompt and run the command:

```testcafe chrome --skip-js-errors js/bookings.ts```

**NOTE** At current the hotels booking test **WILL FAIL** due to an issue with the TestCafe automation library that for some reason cannot click the "Book Now" button on the hotel details page, 
despite finding the button, hovering the mouse over it and sending the click event.

There is a ticket open for this issue with the developers of TestCafe which can be found [here](https://github.com/DevExpress/testcafe/issues/2479)

***
#Extra Points Section#
Due to not having any mac devices i cannot attempt this section, since it requires xcode and apple specific hardware to run.