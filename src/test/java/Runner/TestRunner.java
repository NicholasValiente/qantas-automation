package Runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(features = {"src/test/resources/Features/"},
        glue = {"StepDefinitions"},
        format = {"pretty", "html:target/cucumber", "json:target/cucumber.json"},
        tags = {"~@ignore"})
public class TestRunner {

}