package StepDefinitions;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class StepDefinitions {
    private WebDriver driver;
    private WebDriverWait driverWait;
    private HttpClient httpClient;
    private HttpResponse response;

    @Before
    public void beforeEach(Scenario scenario) {
        System.out.println(String.format("\nExecute Scenario %s\n", scenario.getName()));

        ChromeDriverManager.getInstance().version("2.33").setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("window-size=1280,720");
//        options.addArguments("--headless");

        response = null;
        httpClient = HttpClientBuilder.create().build();
        driver = new ChromeDriver(options);
        driverWait = new WebDriverWait(driver, 10);
        System.out.println("Browser started, executing the test!");
    }

    @After
    public void afterEach(Scenario result) {
        if (result.isFailed()) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            result.embed(screenshot, "image/png");
        }
        String resultMessage = String.format("\nScenario %s Finished with status : %s\n", result.getName(), result.getStatus().toUpperCase());
        System.out.println((resultMessage));
        driver.close();
        driver.quit();
    }


    @Given("^I am on the light bulb challenge page$")
    public void iAmOnTheLightBulbChallengePage() {
        String url = String.format("%s?userId=%s", TestData.lightBulbURL, TestData.userID);
        driver.get(url);
        Assert.assertEquals(driver.getCurrentUrl(), url);
    }


    @When("^I turn the light bulb on$")
    public void iTurnTheLightBulbOn() throws Throwable {
        String url = String.format("%sapi/allmethods/on?userId=%s", TestData.lightBulbURL, TestData.userID);
        HttpPost request = new HttpPost(url);
        request.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        String json = "{\"power\":20}";
        request.setEntity(new StringEntity(json));
        response = httpClient.execute(request);
    }


    @When("^I turn the light bulb off$")
    public void iTurnTheLightBulbOff() throws Throwable {
        String url = String.format("%sapi/allmethods/off?userId=%s", TestData.lightBulbURL, TestData.userID);
        HttpPost request = new HttpPost(url);
        request.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        response = httpClient.execute(request);
    }


    @Then("^I should see the light bulb is off$")
    public void iShouldSeeTheLightBulbHasTurnedOff() {
        Assert.assertEquals(response.getStatusLine().getStatusCode(), 200);
        WebElement glow = driverWait.until(
                ExpectedConditions.visibilityOfElementLocated(
                        By.cssSelector("div[id=lightrays]")));
        Assert.assertTrue(glow.getAttribute("style").contains("background: black"));
    }


    @Then("^I should see the light bulb is on$")
    public void iShouldSeeTheLightBulbIsOn() {
        Assert.assertEquals(response.getStatusLine().getStatusCode(), 200);
        WebElement glow = driverWait.until(
                ExpectedConditions.visibilityOfElementLocated(
                        By.cssSelector("div[id=lightrays]")));
        Assert.assertTrue(glow.getAttribute("style").contains(TestData.lightGlowString + "16.6667%"));
    }


    @When("^I turn the light bulb on to (.*)$")
    public void iTurnTheLightBulbOnToPower(String power) throws Throwable {
        String url = String.format("%sapi/allmethods/on?userId=%s", TestData.lightBulbURL, TestData.userID);
        HttpPost request = new HttpPost(url);
        request.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        JSONObject json = new JSONObject().put("power", power);
        request.setEntity(new StringEntity(json.toString(), ContentType.APPLICATION_JSON));
        response = httpClient.execute(request);
    }


    @Then("^I should get a (\\d+) response$")
    public void iShouldGetAResponse(int expectedResponse) {
        Assert.assertEquals(expectedResponse, response.getStatusLine().getStatusCode());
    }


    @Then("^I should see the light bulb is on at (.*)$")
    public void iShouldSeeTheLightBulbIsOnAtPower(String power) {
        WebElement glow = driverWait.until(
                ExpectedConditions.visibilityOfElementLocated(
                        By.cssSelector("div[id=lightrays]")));
        String glowStyle = glow.getAttribute("style");
        switch (Integer.parseInt(power)) {
            case 1:
                Assert.assertTrue(glowStyle.contains(TestData.lightGlowString + "15%"));
                break;

            case 20:
                Assert.assertTrue(glowStyle.contains(TestData.lightGlowString + "16.6667%"));
                break;

            case 40:
                Assert.assertTrue(glowStyle.contains(TestData.lightGlowString + "33.3333%"));
                break;

            case 60:
                Assert.assertTrue(glowStyle.contains(TestData.lightGlowString + "50%"));
                break;

            default:
                System.err.println(String.format("Error, power:\"%s\" is not a configured number", power));
                Assert.fail();
                break;
        }
    }


}
