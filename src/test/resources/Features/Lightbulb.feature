Feature: Light bulb Rest API regression tests

  
  Scenario: Turn light bulb off
    Given I am on the light bulb challenge page
    When I turn the light bulb off
    Then I should see the light bulb is off


  
  Scenario: Turn light bulb on
    Given I am on the light bulb challenge page
    And I turn the light bulb off
    Then I should see the light bulb is off
    When I turn the light bulb on
    Then I should see the light bulb is on

 
  Scenario Outline: Turn light bulb on to correct <power>
    Given I am on the light bulb challenge page
    When I turn the light bulb off
    Then I should see the light bulb is off
    When I turn the light bulb on to <power>
    Then I should see the light bulb is on at <power>
    And I should get a 200 response
    Examples:
      | power |
      | 1     |
      | 20    |
      | 40    |
      | 60    |


  
  Scenario Outline: Turn light bulb on to incorrect power <power>
    Given I am on the light bulb challenge page
    When I turn the light bulb off
    Then I should see the light bulb is off
    When I turn the light bulb on to <power>
    Then I should get a 400 response

    Examples:
      | power |
      | 61    |
      | 0     |

